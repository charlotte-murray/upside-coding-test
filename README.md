# Coding Test - Upside

## Running the program

From the command line, in the Upside.ISBNValidator folder run:

```bash
dotnet run 123456789X
```

where 123456789X is an ISBN-10 or ISBN-13 value

## Running tests

From the command line, in the Upside.ISBNValidator.Tests.Unit folder run:

```bash
dotnet test
```