﻿using System;
using Upside.ISBNValidator.Services;

namespace Upside.ISBNValidator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Console.WriteLine("No ISBN entered");
                return;
            }

            var isbn = string.Join(" ", args);

            var validationService = IsbnValidatorFactory.GetValidationService(isbn);
            if (validationService == null) { Console.WriteLine("no"); }

            var result = validationService.IsValidIsbn();
         
            if (result)
            {
                Console.WriteLine("yes");
            }
            else
            {
                Console.WriteLine("no");
            }
        }
    }
}
