﻿using System;
namespace Upside.ISBNValidator.Services
{
    public class Isbn13ValidationService : BaseIsbnValidationService
    {
        public Isbn13ValidationService(string isbn) : base(isbn)
        {
            ValidIsbnLength = 13;
        }

        protected override bool IsChecksumValid(string isbn)
        {
            if (isbn == null || isbn.Length != ValidIsbnLength) return false;

            var checksum = isbn.Substring(isbn.Length - 1, 1);
            var remainingIsbn = isbn.Substring(0, isbn.Length - 1);

            var sum = 0;

            for (int n = 0; n < remainingIsbn.Length; n++)
            {
                if (int.TryParse(remainingIsbn[n].ToString(), out int valueAtN))
                {
                    bool isEvenPosition = ((n + 1) % 2) == 0;
                    
                    if (isEvenPosition)
                    {
                        sum += (valueAtN * 3);
                    }
                    else
                    {
                        sum += valueAtN;
                    }
                }
                else
                {
                    // This ISBN has one or more characters in it that are not " " or "-" or a digit
                    // (not including the checksum) so it is not a valid ISBN
                    return false;
                }
            }

            var result1 = sum % 10;
            var result2 = 10 - result1;

            var calculatedChecksum = result2 % 10;

            return calculatedChecksum.ToString() == checksum;
        }
    }
}
