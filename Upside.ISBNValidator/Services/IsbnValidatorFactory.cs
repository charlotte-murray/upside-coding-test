﻿using System;
using System.Text.RegularExpressions;

namespace Upside.ISBNValidator.Services
{
    public static class IsbnValidatorFactory
    {
        public static BaseIsbnValidationService GetValidationService(string isbn)
        {
            // Match any individual character that is a digit or "X" or "x"
            var validCharacterCount = Regex.Matches(isbn, @"[\dXx]{1}").Count;

            switch (validCharacterCount)
            {
                case 10:
                    return new Isbn10ValidationService(isbn);
                case 13:
                    return new Isbn13ValidationService(isbn);
            }
            return null;
        }
    }
}
