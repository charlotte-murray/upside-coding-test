﻿using System;
namespace Upside.ISBNValidator.Services
{
    public abstract class BaseIsbnValidationService
    {
        public BaseIsbnValidationService(string isbn)
        {
            Isbn = isbn;
        }

        protected int ValidIsbnLength { get; set; }
        protected string Isbn { get; set; }

        public bool IsValidIsbn()
        {
            if (Isbn == null) return false;

            var sanitisedIsbn = SanitiseIsbnString(Isbn);

            if (sanitisedIsbn.Length != ValidIsbnLength)
            {
                return false;
            }

            return IsChecksumValid(sanitisedIsbn);
        }

        protected static string SanitiseIsbnString(string isbn)
        {
            return isbn.Replace(" ", "").Replace("-", "");
        }

        protected abstract bool IsChecksumValid(string isbn);
    }
}
