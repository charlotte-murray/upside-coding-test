﻿using System;
namespace Upside.ISBNValidator.Services
{
    public class Isbn10ValidationService : BaseIsbnValidationService
    {
        public Isbn10ValidationService(string isbn) : base(isbn)
        {
            ValidIsbnLength = 10;
        }

        protected override bool IsChecksumValid(string isbn)
        {
            if (isbn == null || isbn.Length != ValidIsbnLength) return false;

            var checksum = isbn.Substring(isbn.Length - 1, 1);
            var remainingIsbn = isbn.Substring(0, isbn.Length - 1);

            var sum = 0;
            
            for (int n = 0; n < remainingIsbn.Length; n++)
            {
                if (int.TryParse(remainingIsbn[n].ToString(), out int valueAtN))
                {
                    sum += (valueAtN * (n + 1));
                }
                else
                {
                    // This ISBN has one or more characters in it that are not " " or "-" or a digit
                    // (not including the checksum) so it is not a valid ISBN
                    return false;
                }
            }

            var numericalModResult = sum % 11;
            string calculatedChecksum = numericalModResult == 10 ? "X" : numericalModResult.ToString();

            return checksum == calculatedChecksum;
        }
    }
}
