﻿using System;
using FluentAssertions;
using Upside.ISBNValidator.Services;
using Xunit;

namespace Upside.ISBNValidator.Tests.Unit.Services
{
    public class IsbnValidatorFactoryTests
    {
        [Theory]
        [InlineData("0471958697")]
        [InlineData("0 471 60695 2")]
        [InlineData("0-470-84525-2")]
        [InlineData("0-321-14653-0")]
        [InlineData("0000000000")]
        [InlineData("000000000X")]
        public void GetValidationService_Isbn10Result(string isbn)
        {
            // ACT
            var validator = IsbnValidatorFactory.GetValidationService(isbn);

            // ASSERT
            validator.Should().BeOfType<Isbn10ValidationService>();
        }

        [Theory]
        [InlineData("9780470059029")]
        [InlineData("978 0 471 48648 0")]
        [InlineData("978 abcde 0 471 48648 0")]
        [InlineData("0000000000000")]
        public void GetValidationService_Isbn13Result(string isbn)
        {
            // ACT
            var validator = IsbnValidatorFactory.GetValidationService(isbn);

            // ASSERT
            validator.Should().BeOfType<Isbn13ValidationService>();
        }

        [Theory]
        [InlineData("09780470059029")]
        [InlineData("0978 0 471 48648 0")]
        [InlineData("0978 abcde 0 471 48648 0")]
        [InlineData("000000000000000")]
        [InlineData("")]
        [InlineData("123")]
        [InlineData("123abc")]
        public void GetValidationService_NullResult(string isbn)
        {
            // ACT
            var validator = IsbnValidatorFactory.GetValidationService(isbn);

            // ASSERT
            validator.Should().BeNull();
        }
    }
}
