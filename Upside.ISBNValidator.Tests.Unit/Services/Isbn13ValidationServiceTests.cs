using System;
using FluentAssertions;
using Upside.ISBNValidator.Services;
using Xunit;

namespace Upside.ISBNValidator.Tests.Unit
{
    public class Isbn13ValidationServiceTests : Isbn13ValidationService
    {
        public Isbn13ValidationServiceTests() : base("") { }

        #region IsValidIsbn

        [Theory]
        [InlineData("9780470059029")]
        [InlineData("978 0 471 48648 0")]
        [InlineData("978-0596809485")]
        [InlineData("978-0-13-149505-0")]
        [InlineData("978-0-262-13472-9")]
        public void IsValidIsbn_Valid(string isbn)
        {
            // ACT
            var service = new Isbn13ValidationService(isbn);
            var result = service.IsValidIsbn();

            // ASSERT
            result.Should().BeTrue();
        }

        [Theory]
        [InlineData("9780470059020")] //incorrect checksum
        [InlineData("978@@0470059029")] //unexpected characters
        [InlineData("978047005902900000")] //input too long
        [InlineData("12345")] //input too short
        [InlineData("")] //empty input
        public void IsValidIsbn_Invalid_IncorrectIsbn(string isbn)
        {
            // ACT
            var service = new Isbn13ValidationService(isbn);
            var result = service.IsValidIsbn();

            // ASSERT
            result.Should().BeFalse();
        }

        [Fact]
        public void IsValidIsbn_Invalid_NullInput()
        {
            // ACT
            var service = new Isbn13ValidationService(null);
            var result = service.IsValidIsbn();

            // ASSERT
            result.Should().BeFalse();
        }

        #endregion

        #region IsChecksumValid

        [Fact]
        public void IsChecksumValid_Valid()
        {
            // ARRANGE
            var input = "9780470059029";

            // ACT
            var result = this.IsChecksumValid(input);

            // ASSERT
            result.Should().BeTrue();
        }

        [Theory]
        [InlineData("97|80470059029")] //extra characters
        [InlineData("")] //empty input
        [InlineData("978047005902990")] //long input
        [InlineData("123790X")] //short input
        public void IsChecksumValid_Invalid_IncorrectIsbn(string isbn)
        {
            // ACT
            var result = this.IsChecksumValid(isbn);

            // ASSERT
            result.Should().BeFalse();
        }

        #endregion
    }
}
