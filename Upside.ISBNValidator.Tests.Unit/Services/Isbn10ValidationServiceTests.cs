using System;
using FluentAssertions;
using Upside.ISBNValidator.Services;
using Xunit;

namespace Upside.ISBNValidator.Tests.Unit
{
    public class Isbn10ValidationServiceTests : Isbn10ValidationService
    {
        public Isbn10ValidationServiceTests() : base("") { }

        #region IsValidIsbn

        [Theory]
        [InlineData("123456789X")]
        [InlineData("0471958697")]
        [InlineData("0 471 60695 2")]
        [InlineData("0-470-84525-2")]
        [InlineData("0-321-14653-0")]
        public void IsValidIsbn_Valid(string isbn)
        {
            // ACT
            var service = new Isbn10ValidationService(isbn);
            var result = service.IsValidIsbn();

            // ASSERT
            result.Should().BeTrue();
        }

        [Theory]
        [InlineData("0-321-14653-2")] //incorrect checksum
        [InlineData("0-321-14hello653-0")] //unexpected characters
        [InlineData("0-321-14653-0123")] //input too long
        [InlineData("0-321")] //input too short
        [InlineData("")] //empty input
        public void IsValidIsbn_Invalid_IncorrectIsbn(string isbn)
        {
            // ACT
            var service = new Isbn10ValidationService(isbn);
            var result = service.IsValidIsbn();

            // ASSERT
            result.Should().BeFalse();
        }

        [Fact]
        public void IsValidIsbn_Invalid_NullInput()
        {
            // ACT
            var service = new Isbn10ValidationService(null);
            var result = service.IsValidIsbn();

            // ASSERT
            result.Should().BeFalse();
        }

        #endregion

        #region IsChecksumValid

        [Fact]
        public void IsChecksumValid_Valid()
        {
            // ARRANGE
            var input = "123456789X";

            // ACT
            var result = this.IsChecksumValid(input);

            // ASSERT
            result.Should().BeTrue();
        }

        [Theory]
        [InlineData("1--23456789X")] //extra characters
        [InlineData("")] //empty input
        [InlineData("123456789790X")] //long input
        [InlineData("123790X")] //short input
        public void IsChecksumValid_Invalid_IncorrectIsbn(string isbn)
        {
            // ACT
            var result = this.IsChecksumValid(isbn);

            // ASSERT
            result.Should().BeFalse();
        }

        #endregion
    }
}
